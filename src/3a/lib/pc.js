import fetch from 'isomorphic-fetch';
import _ from 'lodash';

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Что-то пошло не так:', err);
  });

export function getVolumes(){
  const disks = pc['hdd'];
  const result = {};
  disks.forEach((item) => {
    const disk = item['volume'];
    if (result[disk] === undefined) {
      result[disk] = 0;
    }
    result[disk] += item['size'];
  });
  for (var key in result) {
    if (result.hasOwnProperty(key)) {
      result[key] = `${result[key]}B`;
    }
  }
  return result;
}

export default function getPCAttr(url, res) {
  let attr = pc;
  const params = url.split('/').filter(el => el.length !== 0);
  if (!params) {
    res.status(200).send(res.json(attr));
  }


  for (let param of params) {
    console.log(param + " " + attr.constructor()[param]);
    attr = attr.constructor()[param] == undefined? attr[param]: undefined;
    if (attr === undefined) {
      res.status(404).send('Not Found');
    }
  }
  res.status(200).send(res.json(attr));
}
